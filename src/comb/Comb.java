package comb;

import util.Util;

public class Comb {
  public void sort(int items[], int n, int gap, Util util) {
    boolean swaps = true;
    int i, j;
    for(;gap > 0 || swaps;) {
      swaps = false;
      for(i = 0, j = gap; j < n; i++, j++) {
        if(items[i] > items[j]) {
          util.array_swap(items, i, j);
        }
      }
      gap /= 1.2;
    }
  }
}
