package insert;

import util.Util;

public class Insert {
  public void sort(int items[], int n, int gap, Util util) {
    int i, j;
    for(i = gap; i < n; i++) {
      for(j = i; j > 0; j--) {
        if(items[j-1] > items[j]) {
          util.array_swap(items, j-1, j);
        }
      }
    }
  }
}
