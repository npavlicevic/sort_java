import java.io.*;
import java.util.*;
import util.Util;
import select.Select;

public class MainSelect {
  public static void main(String args[]) {
    Scanner in = new Scanner(System.in);
    int n = in.nextInt();
    int items[] = new int[n];
    Util util = new Util();
    util.array_init(items, n);
    util.array_print(items, n);
    Select select = new Select();
    select.sort(items, n, util);
    util.array_print(items, n);
  }
}
