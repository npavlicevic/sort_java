import java.io.*;
import java.util.*;
import util.Util;
import heap.Heap;

public class MainHeap {
  public static void main(String args[]) {
    Scanner in = new Scanner(System.in);
    int n = in.nextInt();
    int items[] = new int[n];
    Util util = new Util();
    util.array_init(items, n);
    util.array_print(items, n);
    Heap heap = new Heap(n+1);
    heap.sort(items, n, util);
    util.array_print(items, n);
  }
}
