package merge;

import util.Util;

public class Merge {
  public void copy(int dest[], int src[], int from, int to, Util util) {
    util.array_copy(dest, src, from, to);
  }
  public void merge(int temp[], int items[], int lo, int mid, int hi) {
    int i = lo, j = lo, k = mid;
    for(;true;) {
      if(items[i] < items[k]) {
        temp[j++] = items[i++];
      } else {
        temp[j++] = items[k++];
      }
      if(i>=mid) {
        break;
      }
      if(k>=hi) {
        break;
      }
    }
    for(; i < mid; i++) {
      temp[j++] = items[i];
    }
    for(; k < hi; k++) {
      temp[j++] = items[k];
    }
  }
  public void sort(int items[], int temp[], int lo, int hi, Util util) {
    if(hi - lo >= 2) {
      int mid = (lo+hi)/2;
      sort(items, temp, lo, mid, util);
      sort(items, temp, mid, hi, util);
      merge(temp, items, lo, mid, hi);
      copy(items, temp, lo, hi, util);
    }
  }
}
