package heap;

import util.Util;

public class Heap {
  protected int position;
  protected int capacity;
  protected int items[];
  public Heap(int capacity_) {
    position = 1;
    capacity = capacity_;
    items = new int[capacity];
  }
  public void push(int item, Util util) {
    items[position] = item;
    int child = position;
    position++;
    int parent = child / 2;
    for(;parent > 0;) {
      if(items[child] < items[parent]) {
        util.array_swap(items, child, parent);
        child = parent;
        parent /= 2;
      } else {
        break;
      }
    }
  }
  public int pop(Util util) {
    int r = items[1];
    int up = items[--position];
    items[1] = up;
    int parent = 1;
    int left_child;
    int right_child;
    int direction;
    // 0 left 1 right
    for(;true;) {
      left_child = 2 * parent;
      right_child = 2 * parent + 1;
      if(left_child >= position && right_child >= position) {
        break;
      }
      if(right_child >= position) {
        direction = 0;
      } else if(left_child >= position) {
        direction = 1;
      } else if(items[left_child] < items[right_child]) {
        direction = 0;
      } else {
        direction = 1;
      }
      if(direction == 0) {
        if(items[left_child] < items[parent]) {
          util.array_swap(items, left_child, parent);
          parent = left_child;
        } else {
          break;
        }
      } else {
        if(items[right_child] < items[parent]) {
          util.array_swap(items, right_child, parent);
          parent = right_child;
        } else {
          break;
        }
      }
    }
    return r;
  }
  public void sort(int items_[], int n, Util util) {
    int i, item;
    for(i = 0; i < n; i++) {
      push(items_[i], util);
    }
    for(i = 0; i < n; i++) {
      item = pop(util);
      items_[i] = item;
    }
  }
}
