package quick;

import util.Util;

public class Quick {
  public int split_boundries(int items[], int lo, int hi, Util util) {
    int item = items[hi];
    int lo_ = lo;
    int hi_ = hi - 1;
    for(;lo_ <= hi_;) {
      for(;items[lo_] < item;) {
        lo_++;
      }
      for(;items[hi_] > item;) {
        hi_--;
      }
      if(lo_ <= hi_) {
        util.array_swap(items, lo_, hi_);
        lo_++;
        hi_--;
      }
    }
    util.array_swap(items, lo_, hi);
    return lo_;
  }
  public int split(int items[], int lo, int hi, Util util) {
    int item = items[hi];
    int i = lo, j = lo;
    for(; i < hi; i++) {
      if(items[i] < item) {
        util.array_swap(items, i, j);
        j++;
      }
    }
    util.array_swap(items, j, hi);
    return j;
    // TODO no mid here just take rightmost
  }
  public void sort(int items[], int lo, int hi, Util util) {
    if(hi > lo) {
      int mid = split(items, lo, hi, util);
      sort(items, lo, mid-1, util);
      sort(items, mid+1, hi, util);
    }
  }
  public void sort_boundries(int items[], int lo, int hi, Util util) {
    if(hi > lo) {
      int mid = split_boundries(items, lo, hi, util);
      sort_boundries(items, lo, mid-1, util);
      sort_boundries(items, mid+1, hi, util);
    }
  }
  public void sort_double_stack(int items[], int lo, int hi, Util util) {
    int stack_len = 16384;
    int lo__;
    int lo_ = 0;
    int lo_stack[] = new int[stack_len];
    lo_stack[lo_++] = lo;
    int hi__;
    int hi_ = 0;
    int hi_stack[] = new int[stack_len];
    hi_stack[hi_++] = hi;
    int mid;
    for(;lo_ > 0 && hi_ > 0;) {
      lo__ = lo_stack[--lo_];
      hi__ = hi_stack[--hi_];
      if(hi__ < lo__) continue;
      mid = split(items, lo__, hi__, util);
      lo_stack[lo_++] = lo__;
      hi_stack[hi_++] = mid-1;
      lo_stack[lo_++] = mid+1;
      hi_stack[hi_++] = hi__;
    }
  }
}

// Quicksort
// =========
// General rule
// ````````````
//  do
//  loop
//  loop
//
// General description
// ```````````````````
// Imagine a list of values and the median value.
// Partition the list so that values in the left list are less than median
// and values in the right list are greater than median. Repeat the process with two new lists.
// Keep repeating the process until each list has only one element.
// At this point the list is sorted.
//
// This is part of divide and conquer approach.
//
// Divide - Divide a list of values into two lists.
// Conquer - Sort each list individually.
// Collect or combine - Collect two lists and combine them.
//
// Recurrence relation best case
// `````````````````````````````
// T(0) = T(1) = 1 Base case
// T(n) = 2T(n/2) + n
//
// Complexity best case
// ````````````````````
// O(n lg n)
//
// Sequence worst case
// ```````````````````
// same as Bubblesort
//
// Complexity worst case
// `````````````````````
// same as Bubblesort
// =========

// Mergesort
// =========
// General rule
// ````````````
//  loop (partition)
//  loop (partition)
//  do (merge)
//  copy
//
// General description
// ```````````````````
// Imagine a list of values and the median value.
// Partition the list so that values left from median are in left list
// and values to the right are in the right list. Repeat the process with two new lists.
// Keep repeating the process until each list has only one element.
// At this point compare lists and merge them to get ordered list.
//
// This is part of divide and conquer approach.
//
// Recurrence relation best case
// `````````````````````````````
// T(0) = T(1) = 1 Base case
// T(n) = 2T(n/2) + n
//
// Complexity best case
// ````````````````````
// O(n lg n)
//
// Sequence worst case
// ```````````````````
// same as Bubblesort
//
// Complexity worst case
// `````````````````````
// same as Bubblesort
// =========


// Heapsort
// =========
// General rule
// ````````````
// build heap, destroy it
//
// General description
// ```````````````````
// Heap can be represented as a binary tree having nodes ordered according to priority, smallest first, min heap, or largest first, max heap. In case element is inserted, it goes to the end of the heap and bubbled up according // to priority. If it is min heap, it goes if less than parent. If it is max heap, it goes up if greater than parent.
//
// First element from the heap is removed and replaced by last element. This element is bubbled down if less than child or greater than child, depending on if it min heap or max heap.
//
// parent ~ i (parent of a child at i ~ i/2)
// left_child ~ 2*i
// right_child ~ 2*i + 1
// starting from ~ 1

// Min heap property
// `````````````````
// parent is less than left and right child
//
// Max heap property
// `````````````````
// parent is greater than left and right child
//
// Complexity best case
// ````````````````````
// O(n lg n)
//
// swaps on insert O(lg n) for n elements O(n lg n)
//
// Number of elements in the tree
// ``````````````````````````````
// 2`(h+1) - 1
//
// Proof
// `````
// h=0 trivial
// h=0,1,2,3,4,...,n
// h=n+1
// subtrees t1 and t2 of height 2`(n+1) - 1
// h of t 1 + (2`(n+1) - 1) + (2`(n+1) - 1) = 2 2`(n+1) - 1 = 2`(n+2) - 1
//
// Number of edges in the tree
// ```````````````````````````
// n-1 n ~ number of nodes
// =========

// The worst case for recurrence relations
// =======================================
// f(n) = a f(n/b) + h(n)
//
// f(n) ~ time to solve all problems e.g. sort all elements
// a ~ number of subproblems to solve
// f(n/b) ~ b number of splits, f(n/b) time solve subproblem often a=b e.g. sort sublist
// h(n) ~ time taken to collect subproblems e.g. collect sublists
//
// O(log n) a = 1
// O(n`(log_b a)) a > 1
//
// Specific case, master theorem
// `````````````````````````````
// f(n)=af(n/b) + c(n`d)
// O(n`d) a<b`d
// O(n`d log n) a=b`d
// O(n`(log_b a)) a>b`d
// =======================================

// Rules for O notation
// ====================
// O(c)=1 c some constant
// O(cn)=cO(n)=O(n)
// O(n1)+O(n2)=O(n1+n2)=max(O(n1), O(n2)) e.g. O(n`2 + n) = O(n`2)
// O(n1)O(n2)=O(n1n2)
// ====================

// Shellsort
// ==========
// General rule
// ````````````
// select gaps and apply some other algorithm
//
// General description
// ```````````````````
// Shellsort is an iterative algorithm. It divides a list in number of k sorted lists (e.g. 3 sorted, 7 sorted and so on). One class will hold completely sorted list.
// In practice the idea is avoid swaps. The hope is that list will be sorted in a current class and no swaps are required in next class. Any basic algorithm can be used
// with shellsort to achieve the effect.
//
// Sequence worst case
// ``````````````````````````````
// same as Insertsort
//
// Complexity worst case
// `````````````````````
// same as Insertsort
// ==========

// Combsort
// ==========
// General rule
// ````````````
// select gaps and apply bubblesort
//
// General description
// ```````````````````
// Combsort works in the same way as shellsort. The difference is classes are recursively defined instead of predetermined.
//
// Sequence worst case
// ``````````````````````````````
// same as Bubblesort
//
// Complexity worst case
// `````````````````````
// same as Bubblesort
// ==========

// Bubblesort
// ==========
// General rule
// ````````````
// keep running until swaps
// gap 1 can be changed
//
// General description
// ```````````````````
// Bubble sort is iterative algorithms. Basic operation is a swap. Smaller element is interchanged with bigger one or vice versa.
// The algorithm iterates and swaps elements until the list is not ordered.
//
// Sequence worst case
// ``````````````````````````````
// at the start n-1 comparisons, at the end 1
// n-1 + n-2 + n-3 + ... 2 + 1 = n(n-1) / 2 = 1/2 n`2 - 1/2
//
// Complexity worst case
// `````````````````````
// O(n`2)
// ==========

// Insertsort
// ==========
// General rule
// ````````````
// sort up to limit
// from limit backward just like bubble sort
// gap 1 can be changed
//
// General description
// ```````````````````
// Insertion sort is iterative algorithm. It is similar to bubble sort. The algorithm iterates and carries out swaps until a limit.
// The list is considered sorted up to reached limit. At each pass the limit is increased. Once the limit is same as list length the algorithm terminates.
//
// Sequence worst case
// ```````````````````
// number of swaps
// 2+3+4+5+...+ n-3 + n-2 + n-1 = n(n-1)/2 - 1
// Complexity worst case
// `````````````````````
// O(n`2)


// Selectsort
// ==========
// General rule
// ````````````
// find min
//
// General description
// ```````````````````
// Selection sort is iterative algorithm. It divides the list in two parts, one part sorted list and other part// unsorted list. Iterations continues until all elements are in sorted part.
//
// Sequence worst case
// ```````````````````
// number of swaps
// n-1 + n-2 + n-3 + ... + 3 + 2 + 1 = n(n-1) / 2
// Complexity worst case
// `````````````````````
// O(n`2)

