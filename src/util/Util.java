package util;

import java.util.Random;

public class Util {
  public void array_print(int items[], int n) {
    int i = 0;
    for(; i < n; i++) {
      System.out.format("%d ", items[i]);
    }
    System.out.format("\n");
  }
  public void array_init(int items[], int n) {
    int i = 0;
    int max = 1024;
    Random r = new Random();
    for(; i < n; i++) {
      items[i] = r.nextInt(max);
    }
  }
  public void array_copy(int dest[], int src[], int from, int to) {
    int i = from;
    for(; i < to; i++) {
      dest[i] = src[i];
    }
  }
  public void array_swap(int items[], int from, int to) {
    int temp = items[to];
    items[to] = items[from];
    items[from] = temp;
  }
}
