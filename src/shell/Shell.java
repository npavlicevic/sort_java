package shell;

import insert.Insert;
import util.Util;

public class Shell {
  public void sort(int items[], int n, Insert insert, Util util) {
    int gaps_ = 7;
    int gaps[] = new int[]{19, 17, 11, 7, 5, 3, 1};
    int i;
    for(i = 0; i < gaps_; i++) {
      insert.sort(items, n, gaps[i], util);
    }
  }
}
