import java.io.*;
import java.util.*;
import util.Util;
import comb.Comb;

public class MainComb {
  public static void main(String args[]) {
    Scanner in = new Scanner(System.in);
    int n = in.nextInt();
    int items[] = new int[n];
    Util util = new Util();
    util.array_init(items, n);
    util.array_print(items, n);
    Comb comb = new Comb();
    comb.sort(items, n, n-1, util);
    util.array_print(items, n);
  }
}
