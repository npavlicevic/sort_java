import java.io.*;
import java.util.*;
import util.Util;
import merge.Merge;

public class MainMerge {
  public static void main(String args[]) {
    Scanner in = new Scanner(System.in);
    int n = in.nextInt();
    int items[] = new int[n];
    int temp[] = new int[n];
    Util util = new Util();
    util.array_init(items, n);
    util.array_print(items, n);
    Merge merge = new Merge();
    merge.sort(items, temp, 0, n, util);
    util.array_print(items, n);
  }
}
