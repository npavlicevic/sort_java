package select;

import util.Util;

public class Select {
  public void sort(int items[], int n, Util util) {
    int i, j, min;
    for(i = 0; i < n-1; i++) {
      min = i;
      for(j = i+1; j < n; j++) {
        if(items[j] < items[min]) {
          min = j;
        }
      }
      if(min != i) {
        util.array_swap(items, min, i);
      }
    }
  }
}
