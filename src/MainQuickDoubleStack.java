import java.io.*;
import java.util.*;
import util.Util;
import quick.Quick;

public class MainQuickDoubleStack {
  public static void main(String args[]) {
    Scanner in = new Scanner(System.in);
    int n = in.nextInt();
    int items[] = new int[n];
    Util util = new Util();
    util.array_init(items, n);
    util.array_print(items, n);
    Quick quick = new Quick();
    quick.sort_double_stack(items, 0, n-1, util);
    util.array_print(items, n);
  }
}
