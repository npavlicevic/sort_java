import java.io.*;
import java.util.*;
import util.Util;
import insert.Insert;

public class MainInsert {
  public static void main(String args[]) {
    Scanner in = new Scanner(System.in);
    int n = in.nextInt();
    int items[] = new int[n];
    Util util = new Util();
    util.array_init(items, n);
    util.array_print(items, n);
    Insert insert = new Insert();
    insert.sort(items, n, 1, util);
    util.array_print(items, n);
  }
}
