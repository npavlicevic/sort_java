package bubble;

import util.Util;

public class Bubble {
  public void sort(int items[], int n, int gap, Util util) {
    int i, j;
    boolean swaps = true;
    for(;swaps;) {
      swaps  = false;
      for(i = 0, j = gap; j < n; i++, j++) {
        if(items[i] > items[j]) {
          util.array_swap(items, i, j);
          swaps = true;
        }
      }
    }
  }
}
