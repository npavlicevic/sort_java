## sorting algorithms in java
sorting algorithms in java
### algorithms implemented
- select sort
- bubble sort
- insert sort
- shell sort
- comb sort
- quick sort
- merge sort
- heap sort
### compile
make all
### run example
e.g. echo "16" | java -cp classes MainBubble    
start bubble sort with random array of length 16
